package si2_schnabel_masson

import java.io._
import scala.io.Source

object Schnabel_Masson_Main {
  
  /**
   * La connaissance
   * Les noeuds internes representent les questions
   * Les feuilles representent les animaux
   * Arbre toujours plein
   */
  trait ABanimal
  case class Animal(nom:String) extends ABanimal
  case class Question(q:String, oui:ABanimal, non:ABanimal) extends ABanimal
  
  trait CustomException 
  case class FileEmptyException(message:String) extends Exception(message)
  case class FileIncompatibleException(message:String) extends Exception(message)
  
  /**
   * Retourne l'arbre animal correspondant a un fichier
   * 
   * @param nomf	Le nom du fichier
   * @return 			L'arbre animal correspondant
   * 
   * Gere les cas ou le fichier est vide/innexistant et l'arbre est non plein
   */
  def fichierToABanimal(nomf:String):ABanimal  = {
    val l = Source.fromFile(nomf).getLines().toList;
    if(l.size==0) throw new FileEmptyException("Le fichier est vide")
    if(!l(0).startsWith("q:")) throw new FileIncompatibleException("Le fichier doit commencer par une question")
    def parcour(c:List[String], count:Int):ABanimal = c match{
      case Nil => null
      case t::q if(t.startsWith("q:") && count==0) => new Question(t.substring(2),parcour(q,0),parcour(q,1))
      case t::q if(t.startsWith("q:")) => parcour(q,count+1)
      case t::q if(count==0) => new Animal(t)
      case t::q => parcour(q,count-1)
    }
    parcour(l,0)
  }
  
  /**
   * Fonction qui cree un arbre a� partir d'une liste de String (fonction util pour test)
   * 
   * @param c		La liste de String
   * @param count  Le compteur qui permet le parcours de la liste
   * 
   * @return L'arbre animal correspondant
   */
  def parcour(c:List[String], count:Int):ABanimal = c match{
      case Nil => null
      case t::q if(t.startsWith("q:") && count==0) => new Question(t.substring(1),parcour(q,0),parcour(q,1))
      case t::q if(t.startsWith("q:")) => parcour(q,count+1)
      case t::q if(count==0) => new Animal(t)
      case t::q => parcour(q,count-1)
    }
  
  
  /**
   * Stocke un arbre dans un animal
   * 
   * @param nomf	Le nom du fichier
   * @param a			L'arbre a enregistrer
   */
  def ABanimalToFichier(nomf:String, a:ABanimal):Unit = {
    val writer = new FileWriter(new File(nomf)) 
    def parcourirArbre(ab:ABanimal):String = ab match{
      case Question(q,oui,non) => {
        "q:" + q + "\n" + parcourirArbre(oui) + parcourirArbre(non)
      }
      case Animal(n) => n+"\n"
    }
    writer.write(parcourirArbre(a))
    writer.close()
  }
  
  /**
   * Permet de jouer a akinator animal
   * 
   * @param ABanimal	a, l'arbre sur lequel l'ordinateur va questioner l'utilisateur
   * @param it        liste des reponses du joueur
   * @return Boolean	true si l'ordinateur a trouve l'animal de l'utilsateur
   * 									false sinon
   */
  def jeuSimple(ab:ABanimal,it:Iterator[String]):Boolean = {
    print("Pensez a un animal - ")
    def questionner(i:ABanimal):Boolean = i match{
      case Question(q,o,n) =>
        println(q)
        if(it.next().matches("^(o|oui|y|yes)"))
          questionner(o)
        else
          questionner(n)
      case Animal(a) =>
        println("Pensez vous a : "+a)
        if(it.next().matches("^(o|oui|y|yes)")){
          println("J'ai gagne")
          true
        }else{
          println("J'ai perdu")
          false
        }
    }
    questionner(ab)
  }
  
  /**
   * Permet de jouer a akinator animal et de recuperer les logs
   * 
   * @param ABanimal	a, l'arbre sur lequel l'ordinateur va questioner l'utilisateur
   * @param it        liste des reponses du joueur
   * @return Boolean	true si l'ordinateur a trouve l'animal de l'utilsateur
   * 									false sinon
   */
  def jeuLog(ab:ABanimal, it:Iterator[String]):List[String] = {
    print("Pensez a un animal - ")
    val list = Nil
    def questionner(i:ABanimal, l:List[String]):List[String] = i match{
      case Question(q,o,n) =>
        println(q)
        val res = it.next()
        if(res.matches("^(o|oui|y|yes)"))
          questionner(o,res::l)
        else
          questionner(n,res::l)
      case Animal(a) =>
        println("Pensez vous a : "+a)
        val res = it.next()
        if(res.matches("^(o|oui|y|yes)")){
          println("J'ai gagne")
          res::l
        }else{
          println("J'ai perdu")
          res::l
        }
    }
    questionner(ab,list)
  }
  
  /**
   * Permet de jouer a akinator animal et d'ajouter un animal par l'utilisateur si l'ordinateur perd
   * 
   * @param Iterator	it	La suite de reponse de l'utilisateur
   * @param ABanaml		a		L'arbre sur lequel jouer
   * @return ABanimal 		L'arbre avec le nouvel animal si l'ordinateur perd
   */
  def jeuApprentissage(ab:ABanimal, it:Iterator[String]):ABanimal = {
    print("Pensez a un animal - ") 
    def questionner(i:ABanimal):ABanimal = i match{
      case Question(q,o,n) =>
        println(q)
        if(it.next().matches("^(o|oui|y|yes)"))
          new Question(q,questionner(o),n)
        else
          new Question(q,o,questionner(n))
      case Animal(a) =>
        println("Pensez vous a : "+a)
        if(it.next().matches("^(o|oui|y|yes)")){
          println("J'ai gagne")
          new Animal(a)
        }else{
          println("J'ai perdu - quelle est la bonne r�ponse ?")
          val nouvelAnimal = new Animal(it.next());
          println("Quelle question permet de dif�rencier "+nouvelAnimal.nom+" de "+a+" ?")
          val question = it.next();
          println("Quelle est la r�ponse � cette question pour "+nouvelAnimal.nom+" ?")
          if(it.next().matches("^(o|oui|y|yes)")){
            new Question(question, nouvelAnimal, new Animal(a))
          }else{
            new Question(question, new Animal(a),nouvelAnimal)
          }
        }
    }
    questionner(ab)
  }
  
  def jeuSimpleJNSP(a:ABanimal, it:Iterator[String]):Boolean = {
      print("Pensez a un animal - ")
    def questionner(i:ABanimal):Boolean = i match{
      case Question(q,o,n) =>
        println(q)
        val res = it.next()
        if(res.matches("x|jsp|je ne sais pas")) 
          if(!questionner(o)){ questionner(n)}
          else true
        else if(res.matches("^(o|oui|y|yes)"))
          questionner(o)
        else
          questionner(n)
      case Animal(a) =>
        println("Pensez vous a : "+a)
        if(it.next().matches("^(o|oui|y|yes)")){
          true
        }else{
          false
        }
    }
    if(questionner(a)) {println("J'ai gagne"); true} else {println("J'ai perdu"); false}
  }
  
  
  def main(args: Array[String]): Unit = {
    val filename = "Main.txt";
		println("Bienvenue sur Akinimal !");
		val it = Source.stdin.getLines();
		
		def jouer(ab : ABanimal):ABanimal = {
		  val nvAb : ABanimal = jeuApprentissage(ab, it);
		  println("Voulez vous rejouer ?");
		  if(it.next().matches("^(o|oui|y|yes)")){
		    jouer(nvAb);
		  }else{
		    nvAb
		  }
		}
		
		ABanimalToFichier(filename, jouer(fichierToABanimal(filename)))
		
		println("See ya !");
		
    /*val a1 = new Question("L'animal a-t-il des ailes ?", 
        new Question("L'animal a-t-il des plumes ?",
            new Question("Est-ce un animal courant en ville ?",
                new Animal("Piegon"),
                new Animal("Pelican")
            ),
            new Question("Est-ce un animal qui dort la tete en bas ?",
                new Animal("Chauve-Souris"),
                new Animal("Poule Tondu")
            )
        ),
        new Question("Est-ce un animal sauvage",
            new Question("Est-il blanc ?",
                new Animal("Ours Blanc"),
                new Animal("Herison")
            ),
            new Question("L'animal aboit-il ?",
                new Animal("Chien"),
                new Animal("Chat")
            )
        ))          
    ABanimalToFichier(filename,a1);*/
    //ABanimalToFichier("nouveauArbre.txt", jeuApprentissage(fichierToABanimal(filename),Source.stdin.getLines()));
  }
  
}