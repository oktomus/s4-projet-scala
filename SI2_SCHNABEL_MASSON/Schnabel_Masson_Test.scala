package si2_schnabel_masson

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class Schnabel_Masson_Test extends FunSuite{
  
  import Schnabel_Masson_Main._
  import java.io._
  
  val folder = ".";
  
  
  
  test("1. La lecutre du fichier ecrit doit etre identique a l'arbre ecrit"){
    val filename = folder+"/t1.txt"
    val a1 = new Question("L'animal a-t-il des yeux ?", new Animal("Humain"), new Animal("Poule"))
    ABanimalToFichier(filename,a1);    
    val a2 = fichierToABanimal(filename)
    val f = new File(filename)
    f.delete()
    assert(a2==a1);
  }
  
  test("1.1 - Arbre avec 1 question pour oui -  La lecutre du fichier ecrit doit etre identique a l'arbre ecrit"){
    val filename = folder+"/t11.txt"
    val a1 = new Question("L'animal a-t-il des poils ?", 
        new Question("L'animal est-il rose ?",
            new Animal("Ours Rose"),
            new Animal("Ours Brun")
        ), 
        new Animal("Poule"))
    ABanimalToFichier(filename,a1);    
    val a2 = fichierToABanimal(filename)
    val f = new File(filename)
    f.delete()
    assert(a2==a1);
  }
  
  test("1.2 - Arbre avec 1 question pour non -  La lecutre du fichier ecrit doit etre identique a l'arbre ecrit"){
    val filename = folder+"/t12.txt"
    val a1 = new Question("L'animal a-t-il des yeux ?", 
        new Animal("Ornythorinque"),
        new Question("L'animal a-t-il des jambes ?",
            new Animal("Sansoeil jambe"),
            new Animal("Sansoeil sansjambe")
        ))        
    ABanimalToFichier(filename,a1);    
    val a2 = fichierToABanimal(filename)
    val f = new File(filename)
    f.delete()
    assert(a2==a1);
  }
  
  test("1.3 - Arbre complexe de taille 2 -  La lecutre du fichier ecrit doit etre identique a l'arbre ecrit"){
    val filename = folder+"/t13.txt"
    val a1 = new Question("L'animal a-t-il des ailes ?", 
        new Question("L'animal a-t-il des plumes ?",
            new Animal("Pelican"),
            new Animal("Chauve-Souris")
        ),
        new Question("L'animal aboit-il ?",
            new Animal("Chien"),
            new Animal("Chat")
        ))          
    ABanimalToFichier(filename,a1);    
    val a2 = fichierToABanimal(filename)
    val f = new File(filename)
    f.delete()
    assert(a2==a1);
  }
  
  test("1.4 - Arbre complexe de taille 3 -  La lecutre du fichier ecrit doit etre identique a l'arbre ecrit"){
    val filename = folder+"/t14.txt"
    val a1 = new Question("L'animal a-t-il des ailes ?", 
        new Question("L'animal a-t-il des plumes ?",
            new Question("Est-ce un animal courant en ville ?",
                new Animal("Piegon"),
                new Animal("Pelican")
            ),
            new Question("Est-ce un animal qui dort la t�te en bas ?",
                new Animal("Chauve-Souris"),
                new Animal("Poule Tondu")
            )
        ),
        new Question("Est-ce un animal sauvage",
            new Question("Est-il blanc ?",
                new Animal("Ours Blanc"),
                new Animal("Herison")
            ),
            new Question("L'animal aboit-il ?",
                new Animal("Chien"),
                new Animal("Chat")
            )
        ))          
    ABanimalToFichier(filename,a1);    
    val a2 = fichierToABanimal(filename)
    val f = new File(filename)
    f.delete()
    assert(a2==a1);
  }
  
  test("2.1 Lecture d'un fichier inexistant doit retourner une erreur"){    
    intercept[FileNotFoundException]{
    	val a2 = fichierToABanimal(folder+"/inconnueAuBataillon.tstf")      
    }
  }
  
  test("2.2 Lecture d'un fichier fichier vide doit retourner une erreur"){    
    val writer = new FileWriter(new File(folder+"/inconnueAuBataillon.vide"))
    writer.close()
    intercept[FileEmptyException]{
    	val a2 = fichierToABanimal(folder+"/inconnueAuBataillon.vide")      
    }
    val f = new File("test/inconnueAuBataillon.vide")
    f.delete()
  }
  
  test("2.3 Lecture d'un fichier arbre vide ou contenu non traductible retourne une errur"){   
    val writer = new FileWriter(new File(folder+"/inconnueAuBataillon.incompatible")) 
    writer.write("N'importe quoi")
    writer.close()
    intercept[FileIncompatibleException]{
    	val a2 = fichierToABanimal(folder+"/inconnueAuBataillon.incompatible")      
    }
    val f = new File(folder+"/inconnueAuBataillon.incompatible")
    f.delete()
  }
  
  test("4.1 Lancement jeu simple - Reponse fausse doit renvoyer false"){
    val it = "o\no\no\nn".lines
    val a1 = new Question("L'animal a-t-il des ailes ?", 
        new Question("L'animal a-t-il des plumes ?",
            new Question("Est-ce un animal courant en ville ?",
                new Animal("Piegon"),
                new Animal("Pelican")
            ),
            new Question("Est-ce un animal qui dort la t�te en bas ?",
                new Animal("Chauve-Souris"),
                new Animal("Poule Tondu")
            )
        ),
        new Question("Est-ce un animal sauvage",
            new Question("Est-il blanc ?",
                new Animal("Ours Blanc"),
                new Animal("Herison")
            ),
            new Question("L'animal aboit-il ?",
                new Animal("Chien"),
                new Animal("Chat")
            )
        ))
    val res = jeuSimple(a1,it)
    assert(!res)
  }
  
  test("4.2 Lancement jeu simple - Reponse juste doit retourner true"){
    val it = "o\no\no\no".lines
    val a1 = new Question("L'animal a-t-il des ailes ?", 
        new Question("L'animal a-t-il des plumes ?",
            new Question("Est-ce un animal courant en ville ?",
                new Animal("Piegon"),
                new Animal("Pelican")
            ),
            new Question("Est-ce un animal qui dort la tete en bas ?",
                new Animal("Chauve-Souris"),
                new Animal("Poule Tondu")
            )
        ),
        new Question("Est-ce un animal sauvage",
            new Question("Est-il blanc ?",
                new Animal("Ours Blanc"),
                new Animal("Herison")
            ),
            new Question("L'animal aboit-il ?",
                new Animal("Chien"),
                new Animal("Chat")
            )
        ))
    val res = jeuSimple(a1,it)
    assert(res)
  }
  
  test("6.1 jeu par apprentissage, ajout d'un animal a oui"){
    val it = "o\no\no\nn\nMoineau\nMange t-il des frites ?\nn".lines
    val a1 = new Question("L'animal a-t-il des ailes ?", 
              new Question("L'animal a-t-il des plumes ?",
                  new Question("Est-ce un animal courant en ville ?",
                      new Animal("Piegon"),
                      new Animal("Pelican")
                  ),
                  new Question("Est-ce un animal qui dort la tete en bas ?",
                      new Animal("Chauve-Souris"),
                      new Animal("Poule Tondu")
                  )
              ),
              new Question("Est-ce un animal sauvage",
                  new Question("Est-il blanc ?",
                      new Animal("Ours Blanc"),
                      new Animal("Herison")
                  ),
                  new Question("L'animal aboit-il ?",
                      new Animal("Chien"),
                      new Animal("Chat")
                  )
              ));
    val resAttendu = new Question("L'animal a-t-il des ailes ?", 
              new Question("L'animal a-t-il des plumes ?",
                  new Question("Est-ce un animal courant en ville ?",
                      new Question("Mange t-il des frites ?",
                          new Animal("Piegon"),
                          new Animal("Moineau")
                      ),
                      new Animal("Pelican")
                  ),
                  new Question("Est-ce un animal qui dort la tete en bas ?",
                      new Animal("Chauve-Souris"),
                      new Animal("Poule Tondu")
                  )
              ),
              new Question("Est-ce un animal sauvage",
                  new Question("Est-il blanc ?",
                      new Animal("Ours Blanc"),
                      new Animal("Herison")
                  ),
                  new Question("L'animal aboit-il ?",
                      new Animal("Chien"),
                      new Animal("Chat")
                  )
              ));
    val res = jeuApprentissage(a1,it)
    assert(res==resAttendu)
  }
  
  test("6.2 jeu par apprentissage, ajout d'un animal a non"){
    val it = "o\no\no\nn\nMoineau\nEst-il noir ?\no".lines
    val a1 = new Question("L'animal a-t-il des ailes ?", 
              new Question("L'animal a-t-il des plumes ?",
                  new Question("Est-ce un animal courant en ville ?",
                      new Animal("Piegon"),
                      new Animal("Pelican")
                  ),
                  new Question("Est-ce un animal qui dort la tete en bas ?",
                      new Animal("Chauve-Souris"),
                      new Animal("Poule Tondu")
                  )
              ),
              new Question("Est-ce un animal sauvage",
                  new Question("Est-il blanc ?",
                      new Animal("Ours Blanc"),
                      new Animal("Herison")
                  ),
                  new Question("L'animal aboit-il ?",
                      new Animal("Chien"),
                      new Animal("Chat")
                  )
              ));
    val resAttendu = new Question("L'animal a-t-il des ailes ?", 
              new Question("L'animal a-t-il des plumes ?",
                  new Question("Est-ce un animal courant en ville ?",
                      new Question("Est-il noir ?",
                          new Animal("Moineau"),
                          new Animal("Piegon")
                      ),
                      new Animal("Pelican")
                  ),
                  new Question("Est-ce un animal qui dort la tete en bas ?",
                      new Animal("Chauve-Souris"),
                      new Animal("Poule Tondu")
                  )
              ),
              new Question("Est-ce un animal sauvage",
                  new Question("Est-il blanc ?",
                      new Animal("Ours Blanc"),
                      new Animal("Herison")
                  ),
                  new Question("L'animal aboit-il ?",
                      new Animal("Chien"),
                      new Animal("Chat")
                  )
              ));
    val res = jeuApprentissage(a1,it)
    assert(res==resAttendu)
  }
  

  test("7.1 - lancement jeuSimpleJNSP"){
    val it = "o\no\nx\nn\no".lines
    val a1 = new Question("L'animal a-t-il des ailes ?", 
              new Question("L'animal a-t-il des plumes ?",
                  new Question("Est-ce un animal courant en ville ?",
                      new Animal("Piegon"),
                      new Animal("Pelican")
                  ),
                  new Question("Est-ce un animal qui dort la tete en bas ?",
                      new Animal("Chauve-Souris"),
                      new Animal("Poule Tondu")
                  )
              ),
              new Question("Est-ce un animal sauvage",
                  new Question("Est-il blanc ?",
                      new Animal("Ours Blanc"),
                      new Animal("Herison")
                  ),
                  new Question("L'animal aboit-il ?",
                      new Animal("Chien"),
                      new Animal("Chat")
                  )
              ));
    val res = jeuSimpleJNSP(a1,it)
    assert(res)
  }


  
  
}